# UFW - uncomplicated firewall 

> Cheatsheet on UFW by zer0the0ry

----

**Important:** Most ufw commands require sudo.


```
ufw status
```
Displays whether ufw is up and running or not.

```
ufw status verbose
```
As above but with more output.

```
ufw status numbered
```
See the rules numbered for easier management.

```
ufw show
```
Display the current running rules on your firewall.

```
ufw app list
```
See a list of available services.

```
ufw reload
```
Reloads the firewall, which is needed for updating status rules.

```
ufw enable
```
Activates and enables ufw at startup.

```
ufw delete (num)
```
Removes a rule from the numbered status command above.

```
ufw allow 22/tcp
```
Enable port 22 for ssh. 

```
ufw deny 80/tcp
```
Disables port 80 connections.

```
ufw deny in on eth0 from (ip address)
```
Denies incoming traffic on certain interface from certain IP.

```
ufw disable 
```
Stop and disable ufw on startup.

```
ufw reset
```
Resets all the rules to default. Should do while disabled.

```
ufw deny 200:250/tcp
```
Denies all traffic between a certain port range.

```
ufw deny 4444
```
Blocks all TCP and UDP connections on port 4444

```
ufw allow 4444
```
Allows all TCP and UDP on port 4444

```
ufw allow from 10.10.10.10
```
Allow connections from a specific IP.

```
ufw deny from 10.10.10.10
```
Deny connections from a specific IP.

```
ufw allow in 80 
```
Allow incoming connections on port 80.

```
ufw deny out 80
```
Deny outgoing connections on port 80.

```
ufw allow from 10.10.10.10 to any port 80
```
Allow access to port 80 from a specific IP address.

```
ufw allow from 10.10.10.10 proto tcp to any port 80,4444
```
Allow access to port 80 and 4444 on tcp from a specific IP address.

```
ufw deny proto udp from any to any port 22 
```
Deny all UDP traffic to port 22.

```
ufw limit ssh
```
Limit ssh attempts to 6 within 30 seconds.

```
ufw (rule) comment 'your comment here'
```
Add comments to rules.

```
ufw logging on
```
To turn on logging.

```
ufw allow http
```
Another way to enable http.

```
ufw allow "Apache Full"
```
Allows access to apache on port 80 and 443. 

```
ufw allow in on eth0 to any port 80
```
Command for the network interface to have port 80 open in cases of multiple network interfaces. 


