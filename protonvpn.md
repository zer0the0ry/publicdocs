# ProtonVPN Command Line Commands

> May 10Th 2020 

----

## List of all commands

`protonvpn init` Initialize ProtonVPN profile.
`protonvpn connect, c <server>` Select a ProtonVPN server to connect to.
`protonvpn c -r` Connect to a random server. 
`protonvpn c -f` Connect to fastest server.
`protonvpn c --p2p` Connect to fastest P2P server.
`protonvpn c --cc <country code>` Connect to fastest server in specified country.
`protonvpn c --sc` Connect to secure core server.
`protonvpn reconnect, r` Reconnect to to last used server.
`protonvpn disconnect, d` Disconnect from server.
`protonvpn status, s` Print connection status.
`protonvpn configure` Change CLI configuration.
`protonvpn refresh` Refresh OpenVPN configuration and server data.
`protonvpn examples` Print example commands.
`protonvpn --version` Print version.
`protonvpn --help` Show help message.

All connect options can be used with the -p flag to explicitly specify which transmission protocol is used for that connection (either udp or tcp).

## Enabling Kill Switch

To enable kill switch open the configuration menu with `protonvpn configure` then select `5` for kill switch and confirm the activation with either `1` or `2` depending on your preference. 

**1** Will block access from your directly connected network (e.g. public WiFi) and is recommended for laptops that may connect to untrusted networks.
**2** Will allow access from your directly connected network and is for computers that don't leave a secure and trusted LAN, like your home network.
On the next connection kill switch will be enabled.

## Update

To update ProtonVPN-CLI use `sudo pip3 install protonvpn-cli --upgrade`
